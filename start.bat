@echo off
setlocal

:: Get the directory of the currently running script
set SCRIPT_DIR=%~dp0

:: Set the environment variable for the printer IP
set PRINTER_IP=192.168.1.2

:: Create log folder if it doesn't exist
if not exist "%SCRIPT_DIR%logs" mkdir "%SCRIPT_DIR%logs"

:: Log the start time
echo [%date% %time%] Starting Flask app >> "%SCRIPT_DIR%logs\flask_app.log"

:: Change to the script directory
cd /d "%SCRIPT_DIR%"

:: Activate the virtual environment and log the action
call .venv\Scripts\activate >> "%SCRIPT_DIR%logs\flask_app.log" 2>&1

:: Change to the application subdirectory and log the action
cd hope-zebralabel-master >> "%SCRIPT_DIR%logs\flask_app.log" 2>&1

:: Run the Flask application and log the output
python main.py >> "%SCRIPT_DIR%logs\flask_app.log" 2>&1

:: Log the end time
echo [%date% %time%] Flask app started >> "%SCRIPT_DIR%logs\flask_app.log"

endlocal
