# REST API example application

This is an application used to print labels.  It is specifically made for the organization HOPE.  The application will only work with Zebra printers.

# Install
## Setup
1.  Copy down this repo either with Git or a Compressed File
2.  cd into this folder
3.  Create the Virtual Environment
`python -m venv .venv`
4.  Activate the virtual environment
`.venv\Scripts\activate`
5.  Install required libraries
`pip install -r requirements.txt`
6.  Launch the batch script
`./start.bat`
At this point the driver/web server is running

## Reccommended Deployment
This is typically installed on a Windows Server.  I reccommend a scheduled task to launch it.
1.  Open the Task Scheduler application. You can search for it in the Start menu.
2.  In the Task Scheduler, click on Create Basic Task... on the right-hand side.
3.  Give your task a name (e.g., "Run Zebra Driver") and click Next.
4.  Select When the computer starts and click Next.
5.  Choose Start a program and click Next.
6.  Click Browse... and select the batch script in this project (start.bat).
7.  Click Next and then Finish to create the scheduled task.
8.  Re-open the scheduled task
9.  Update the Run As to be SYSTEM
10.  In the settings uncheck "Stop the task of it runs longer than:"

The server can now be stopped and started with this scheduled task.

## Set Printer IP
Inside the batch script there is an environment variable for the printer (PRINTER_IP)
Set this to whatever your printer IP is.

# REST API

The REST API to the app is described below.

All data sent to the app needs to formated in 'application/json' format in order to be interpreted properly.

## Test if running

### Request

`GET /test`

Commonly used to ensure the application is running.  Returns a 200 Code if running.  This endpoint does not require and headers or data.

#### Data

    {}

#### Response

    {"success": "hope-zebralabel is running."}

## Print a RX Label

### Request

`POST /rxlabelprint`

This endpoint will print a prescription label.  Some fields are required, others are not.  If "dry-run" is set to True, the request will be processed, but a label not be send to the printer.  This is useful for testing. 
#### Data


    {  	
       	"name": "John Doe",
        "date": "01/09/1988",
        "directions": "take twice daily with milk",
        "diagnosis": "for pain",
        "script_info": "400mg ibuprofen",
        "dry-run": "True"                            
    }

#### Response

    {"sucess": "label successfully printed"}
