# 3rd Party imports
from flask import Flask
from flask_restful import Api

# Homegrown Imports
from resources.test import Test
from resources.rxlabel import RxLabel

app = Flask(__name__)
api = Api(app)

api.add_resource(RxLabel, '/rxlabelprint')
api.add_resource(Test, '/test')

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8083)
