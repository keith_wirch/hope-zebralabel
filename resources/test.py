from flask_restful import Resource

class Test(Resource):  # Used for testing to see if the app is running.
    def get(self):
        return {'success': 'hope-zebralabel is running version 2.1'}