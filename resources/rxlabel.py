from flask_restful import Resource, reqparse
import socket
import zpl
from PIL import Image
import os
import csv # Used for hex special characters
import logging
from flask import request

# Configure logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

# Endpoint /rxlabelprint
class RxLabel(Resource):
    # Setup Parser to check fields
    parser = reqparse.RequestParser()
    parser.add_argument('line1', type=str)
    parser.add_argument('line2', type=str)
    parser.add_argument('line3', type=str)
    parser.add_argument('line4', type=str)
    parser.add_argument('line5', type=str)
    parser.add_argument('line6', type=str)
    parser.add_argument('line7', type=str)
    parser.add_argument('line8', type=str)
    parser.add_argument('dry-run', type=str)

    # Current Height 
    height = 2

    def post(self):
        try:
            # Log the incoming request
            logger.info("Received a POST request.")

            # Creates a list of strings that will later be converted to hex
            string_collection = []

            # Check if Content-Type is application/json
            if request.content_type == 'application/json':
                data = self.parser.parse_args()  # Captured data from POST request
            else:
                # Handle non-JSON content types
                data = {}
                for arg in self.parser.args:
                    data[arg.name] = request.form.get(arg.name)

            def split(str, num): # Used to split up a string
                list = []
                start = 0 # Position in string splitter
                end = 0
                while start < len(str):
                    end = start + num
                    if end > len(str):  # If the rest of the string is less than the num, just get rest and be done.
                        list.append(str[start:])
                        break
                    cur_str = str[start:end]
                    while not cur_str.endswith(' '): # While the captured string does not end with a space, keep iterating back until you find one.
                        end += -1
                        cur_str = str[start:end]
                    list.append(str[start:end])
                    start = end
                return list

            # Label Settings
            label_width = 101
            label_height = 101
            char_height = 4
            char_width = 3

            l = zpl.Label(label_height, label_width)

            # HOPE Logo
            image_height = 10
            image_width = image_height * 2.210884354  # Based on the dimensions of the image.
            x = (label_width /2) - (image_width /2)  # Used to position image on center horizontally
            l.origin(x, self.height)  # Center Image on Label
            l.write_graphic(Image.open('assets/HOPE_LOGO_BLACK_2.png'), image_width, height=image_height)
            l.endorigin()

            def addline(s):
                self.height += (char_height + 2)
                max_string_length = int((label_width/char_width) + 20)  # Used to break up the string to allow for multiline.
                lines = split(s, max_string_length)
                for line in lines:
                    string_collection.append(line)
                    l.origin(2, self.height)
                    l.write_text(line, char_height=char_height, char_width=char_width, line_width=(label_width-2), justification='L')
                    self.height += char_height
                    l.endorigin()

            for i in range(1, 9):
                line_key = f'line{i}'
                if data[line_key] is not None:
                    addline(data[line_key])
                else:
                    addline('  ')

            output = l.dumpZPL()

            # Replace all Input with the Hex Varient to allow for international characters
            def zpl_hex(string):  # Converts the string into the proper hex for ZPL
                result = ''
                for char in string:
                    hex_code = char.encode('utf-8').hex()
                    if len(hex_code) == 4:
                        hex_code = hex_code[:2] + '_' + hex_code[2:]
                        result += '_' + hex_code
                    else:
                        result += '_' + hex_code
                return result

            for s in string_collection:
                output = output.replace(s, zpl_hex(s))

            # Add support for Hex in the zpl fields
            output = output.replace('^FD', '^FH^FD')

            # Start Printing
            if data.get('dry-run') != "True":
                logger.info(f'Attempting Print to {os.getenv("PRINTER_IP")}')
                mysocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                host = os.getenv("PRINTER_IP")
                port = 9100
                try:
                    mysocket.connect((host, port)) #connecting to host
                    mysocket.send(output.encode())#using bytes
                    mysocket.close() #closing connection
                    logger.info("Print job successfully sent.")
                except Exception as e:
                    logger.error(f"Unable to print. Error: {e}")
                    return {'error': "Unable to print. Error from Print execution is : " + str(e)}, 500
            else:
                logger.info("Dry run mode. Output preview:")
                print(output)
                l.preview()

            return {'success': 'label successfully printed'}

        except Exception as e:
            logger.error(f"An error occurred: {e}", exc_info=True)
            return {'error': "An error occurred. Please check the server logs for more details."}, 500


class SimpleRxLabel(Resource):
    # Setup Parser to check fields
    parser = reqparse.RequestParser()
    parser.add_argument('name', type=str)
    parser.add_argument('dob', type=str)
    parser.add_argument('directions', type=str)
    parser.add_argument('med-details', type=str)
    parser.add_argument('dry-run', type=str)

    def post(self):
        data = self.parser.parse_args()  # Captured data from POST request

        # Label Settings
        label_width = 101
        label_height = 101
        char_height = 4
        char_width = 3
