from flask_restful import Resource, reqparse
import socket
import zpl
from PIL import Image
import os
import csv # Used for hex special characters

# Endpoint /rxlabelprint
class RxLabel(Resource):
    #Setup Parser to check fields
    parser = reqparse.RequestParser()

    parser.add_argument('name',type=str)

    parser.add_argument('dob',type=str)

    parser.add_argument('directions', type=str)

    parser.add_argument('med-details', type=str)
    
    parser.add_argument('dry-run', type=str)

    def post(self):
        data = self.parser.parse_args()  # Captured data from POST request

        hex_list = []
        with open('assets/hex_chars.csv', 'r') as hexcodes:
            hexreader = csv.reader(hexcodes, delimiter=',')
            for row in hexreader:
                hex_list.append(row)

        def fix_special_chars(str):
            pos = 0
            str = str.replace('^FD','^FH^FD')  # Add ^FH (Field Hexadecimal Indicator) to all ^FD areas 
            for char in str:
                global hex_list
                for i in hex_list:
                    if char == i[1]:
                        str = str.replace(char, '_'+i[2]+'_'+i[3])
            return str
                

        # Label Settings
        label_width = 101
        label_height = 101
        char_height = 4
        char_width = 3

        
        def split(str, num): # Used to split up a string
            list = []
            start = 0 # Position in string splitter
            end = 0
            while start < len(str):
                end = start + num
                if end > len(str):  # If the rest of the string is less than the num, just get rest and be done.
                    list.append(str[start:])
                    break
                cur_str = str[start:end]
                while not cur_str.endswith(' '): # While the captured string does not end with a space, keep iterating back until you find one.
                    end += -1
                    cur_str = str[start:end]
                list.append(str[start:end])
                start = end
            #print (list)
            return list


        l = zpl.Label(label_height, label_width,)
        # HOPE Logo
        height = 2
        image_height = 10
        image_width = image_height * 2.210884354  # Based on the dimensions of the image.
        x = (label_width /2) - (image_width /2)  # Used to position image on center horizontally
        l.origin(x,height)  # Center Image on Label
        l.write_graphic(Image.open('assets/HOPE_LOGO_BLACK_2.png'), image_width,height=image_height)
        l.endorigin()

        # Patient Name
        if data['name'] != None:
            height += (char_height + 6)
            max_string_length = int((label_width/char_width)+ 20)  # Used to break up the string to allow for multiline.
            lines = split(data['name'], max_string_length)
            for line in lines:
                l.origin(2, height)
                l.write_text(line, char_height=char_height, char_width=char_width, line_width=(label_width-2), justification='L', font='E:AH.FNT')
                height += char_height
                l.endorigin()
            #height += (image_height + 6)
            #l.origin(2,height)
            #l.write_text(data['name'], char_height=char_height, char_width=char_width, line_width=(label_width-2),
            #    justification='L', font='E:AH.FNT')
            #l.endorigin()

        # Date of Birth
        if data['dob'] != None:
            height += (char_height + 3)
            max_string_length = int((label_width/char_width)+ 20)  # Used to break up the string to allow for multiline.
            lines = split(data['dob'], max_string_length)
            for line in lines:
                l.origin(2, height)
                l.write_text(line, char_height=char_height, char_width=char_width, line_width=(label_width-2), justification='L', font='E:AH.FNT')
                height += char_height
                l.endorigin()
            #height += (char_height + 3)
            #l.origin(2, height)
            #l.write_text(data['dob'], char_height=char_height, char_width=char_width, line_width=(label_width-2),
            #    justification='L', font='E:AH.FNT')
            #l.endorigin()

        # Directions
        if data['directions'] != None:  # Skip if Directions do not exist.
            height += (char_height + 3)
            max_string_length = int((label_width/char_width)+ 20)  # Used to break up the string to allow for multiline.
            lines = split(data['directions'], max_string_length)
            for line in lines:
                l.origin(2, height)
                l.write_text(line, char_height=char_height, char_width=char_width, line_width=(label_width-2), justification='L', font='E:AH.FNT')
                height += char_height
                l.endorigin()

        # Medication Deatils
        if data['med-details'] != None:
            height += (char_height + 3)
            max_string_length = int((label_width/char_width)+ 20)  # Used to break up the string to allow for multiline.
            lines = split(data['med-details'], max_string_length)
            for line in lines:
                l.origin(2, height)
                l.write_text(line, char_height=char_height, char_width=char_width, line_width=(label_width-2), justification='L', font='E:AH.FNT')
                height += char_height
                l.endorigin()

        output = l.dumpZPL()
        #print(output)


        # Start Printing

        printer_socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        host = os.environ['PRINTER_IP']
        #host = '192.168.1.2'
        port = 9100

        if data['dry-run'] != "True":
            try:           
                printer_socket.connect((host, port)) #connecting to host
                printer_socket.send(output.encode('utf-8'))#using bytes
                printer_socket.close () #closing connection
            except Exception as e:
                return {'error': "Unable to print.  Error from Print execution is : "+str(e)}, 500
        
        return {'sucess': 'label successfully printed'}
